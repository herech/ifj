/* ******************************* parser.h ********************************* */
/* Soubor:              parser.h - Hlavickovy soubor pro                      */
/*                                    Syntakticky analyzator (parser)         */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim K�ek            xkrekr00                        */
/*                      Pavel Juha��k         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kova��k        xkovar70                        */
/* ************************************************************************** */

#ifndef EXPR_H_INCLUDED
#define EXPR_H_INCLUDED

#include "ial.h"

/*** Definice konstant pro precedencni tabulku ***/
#define PREC_TABLE_SIZE 17
#define PREC_RULES 15

#define PREC_EQUAL      0
#define PREC_NOTEQUAL   1
#define PREC_LESS       2
#define PREC_GREATER    3
#define PREC_LESS_E     4
#define PREC_GREATER_E  5
#define PREC_PLUS       6
#define PREC_MINUS      7
#define PREC_MUL        8
#define PREC_DIV        9
#define PREC_BRACKET_L  10
#define PREC_BRACKET_R  11
#define PREC_BOOLEAN    12
#define PREC_EOF        13
#define PREC_STRING     14
#define PREC_NUMBER     15
#define PREC_EXPRESSION 16

#define ST_LESS         100
#define ST_GREATER      101
#define ST_EQUAL        102
#define ST_CHECK		103
#define ST_ERROR        -1
#define ST_OK            1
/*************************************************/

#define STACK_SIZE  500

typedef struct precData{
    int tokenType;
    int dataType;
    int value;
    Data *data;

    struct precData *ptr;
}precData;

typedef struct precStack{
    //precData d[STACK_SIZE];
	struct precData *first;
    struct precData *top;
    struct precData *terminalTop;
}precStack;

int precedenceAnalyze(table *localTable, table *globalTable, int ctx, int expectedDataType, int *lastTok, Data *variableData, tListItem* firstGenerInstruc);
#endif // EXPR_H_INCLUDED
