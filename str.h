/* ********************************* str.h ********************************** */
/* Soubor:              str.h - Hlavickovy soubor pro praci s retezci         */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#ifndef STR_H_INCLUDED
#define STR_H_INCLUDED

#define STR_ERROR   1  // chyba pri praci s retezcem
#define STR_SUCCESS 0

typedef struct { // struktura pro reprezentaci retezce
  char* str;     // retezec ukonceny znakem '\0'
  int length;    // skutecna delka retezce
  int allocSize; // velikost alokovane pameti
} string;

int  strInit(string *s); // inicializuje strukturu
void strFree(string *s); // uvolni strukturu

void strClr(string *s);                 // smaze retezec
int  strCat(string *s, char c);         // prida znak v "c" na konec "s"
int  strCatChar(string *s, char *c);    // prida retezec v "c" na konec "s"
int  strCatStr(string *s1, string *s2); // prida obsah "s2" na konec "s1"
int  strCpy(string *s1, string *s2);    // zkopiruje obsah "s2" do "s1"

int  strCmp(string *s1, string *s2);    // porovna "s1" s "s2" (s1 == s2 -> 0)
int  strCmpConst(string *s1, char *s2); // porovna "s1" s "s2" (s1 == s2 -> 0)

char *strGet(string *s); // vrati retezec
int  strLen(string *s);  // vrati delku retezce
int  strSize(string *s); // vrati velikost alokovane pameti

#endif //STR_H_INCLUDED
