/* ****************************** interpret.h ******************************* */
/* Soubor:              interpret.h - Hlavickovy soubor pro                   */
/*                                      Interpter vnitrniho kodu              */
/* Kodovani:            UTF-8                                                 */
/* Datum:               listopad.2014                                         */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#ifndef INTERPRET_H_INCLUDED
#define INTERPRET_H_INCLUDED

#include "instructions.h"
#include "constants.h"
#include "ial.h"
#include "str.h"

/**
* Interpretuje 3AK
* @param  *t   Ukazatel na tabulku symbolu
* @param  *l   Ukazatel na seznam instrukci
* @param  *m   Ukazatel na prvni instrukci fce Main()
* @return  0   Vse v poradku
* @return  6   Behova chyba - chyba nacteni cisla ze vstupu
* @return  7   Behova chyba - Uninicialized variable
* @return  8   Behova chyba - Division by zero
* @return  9   Behova chyba - Else
* @return  99  Interni chyba
*/
int interpret(table *t, tListOfInstr *l, struct listItem *m);

#endif /*INTERPRET_H_INCLUDED*/
