/* ***************************** instructions.c ***************************** */
/* Soubor:              instructions.c - Soubor pro praci s                   */
/*                                         Zretezenym seznamem instrukci      */
/* Kodovani:            UTF-8                                                 */
/* Datum:               listopad.2014                                         */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#include "instructions.h"
#include <stdio.h>
#include <malloc.h>

/* ******************* Fce pro zretezeny seznam instrukci ******************* */
/**
* Inicializuje seznam instrukci
* @param  *L  Ukazatel na seznam instrukci
*/
void listInit(tListOfInstr *L){
  L->first  = NULL;
  L->last   = NULL;
  L->active = NULL;
}

/**
* Uvolni seznam instrukci
* @param  *L  Ukazatel na seznam instrukci
*/
void listFree(tListOfInstr *L){
  tListItem *ptr;
  while (L->first != NULL){
    ptr = L->first;
    L->first = L->first->nextItem;
    free(ptr); // uvolnime celou polozku
  }
}

/**
* Vlozi instrukci na konec seznamu
* @param  *L  Ukazatel na seznam instrukci
* @param   I  Vkladana instrukce
*/
void listInsertLast(tListOfInstr *L, tInstr I){
  tListItem *newItem;
  newItem = malloc(sizeof (tListItem));
  newItem->Instruction = I;
  newItem->nextItem = NULL;

  if (L->first == NULL)
     L->first = newItem;
  else
     L->last->nextItem=newItem;

  L->last=newItem;
}

/**
* Nastavi active na prvni instrukci
* @param  *L  Ukazatel na seznam instrukci
*/
void listFirst(tListOfInstr *L){
  L->active = L->first;
}

/**
* Nastavi active na dalsi instrukci
* @param  *L  Ukazatel na seznam instrukci
*/
void listNext(tListOfInstr *L){
  if (L->active != NULL)
  L->active = L->active->nextItem;
}

/**
* Nastavi active na instrukci urcenou parametrem "gotoInstr"
* @param  *L          Ukazatel na seznam instrukci
* @param  *gotoInstr  Ukazatel na instrukci na kterou chci prejit
*/
void listGoto(tListOfInstr *L, void *gotoInstr){
  L->active = (tListItem*) gotoInstr;
}

/**
* Vrati ukazatel na aktivni instrukci
* @param  *L        Ukazatel na seznam instrukci
* @return  NULL     Pokud (L->active == NULL)
* @return  tInstr*  Ukazatel na instrukci
*/
tInstr *listGetData(tListOfInstr *L){
  if (L->active == NULL)
    return NULL;
  return &(L->active->Instruction);
}

/* ************************* Fce pro zasobnik ramcu ************************* */
/**
* Inicializuje zasobnik ramcu
* @param  *stack  Ukazatel na zasobnik ramcu
*/
void instStackInit(tStack *stack){
  stack->top = NULL;
}

/**
* Uvolni zasobnik ramcu i ramce samotne
* @param  *stack  Ukazatel na zasobnik ramcu
*/
void instStackFree(tStack *stack){
  tStackItem *item;
  while (stack->top != NULL){
    item = stack->top;
    stack->top = stack->top->next;
    deleteTable(item->addr);
    free(item->addr);
    free(item);
  }
}

/**
* Vlozi ramec na vrchol zasobniku
* @param  *stack  Ukazatel na zasobnik ramcu
* @param  *addr   Ukazatel na vkladany ramec
* @return  0      Pri uspechu
* @return  1      Pri neuspesne alokaci
*/
int push(tStack *stack, table *addr){
  tStackItem *newItem;
  if(newItem = malloc(sizeof (tStackItem))){
    newItem->addr = addr;
    newItem->next = stack->top;
    stack->top = newItem;
    return INST_SUCCESS;
  }
  return INST_ERROR;
}

/**
* Uvolni ramec a jeho polozku zasobniku
* @param  *item  Ukazatel na polozku zasoniku
*/
void itemFree(tStackItem *item){
  if(item != NULL){
    deleteTable(item->addr);
    free(item->addr);
    free(item);
  }
}

/**
* Odstrani ramec ze zasobniku
* @param  *stack        Ukazatel na zasobnik ramcu
* @return  tStackItem*  Ukazatel na odstranenou polozku zasoniku
*/
tStackItem *pop(tStack *stack){
  tStackItem *tmp;
  if(stack->top != NULL){
    tmp = stack->top;
    stack->top = stack->top->next;
    return tmp;
  }
}

/**
* Vrati ukazatel na ramec na vrcholu zasobniku
* @param  *stack   Ukazatel na zasobnik ramcu
* @return  table*  Ukazatel na ramec na vrcholu zasobniku
*/
table *top(tStack *stack){
  if(stack->top != NULL)
    return stack->top->addr;
}

/**
* Otestuje jestli je zasobnik prazdny
* @param  *stack  Ukazatel na zasobnik ramcu
* @return  true   Pokud je zasobnik prazdny
* @return  false  Pokud zasobnik neni prazdny
*/
bool empty(tStack *stack){
  if(stack->top == NULL)
    return true;
  return false;
}

/* **************** Funkce pro práci se zásobníkem instrukcí **************** */
/**
* Inicializuje zásobník instrukcí
* @param stack Ukazatel na zásobník instrukcí
* @return void
*/
void instructStackInit(tInstructStack *stack){
  stack->top = NULL;
}

/**
* Uvolní zásobník instrukcí
* @param stack Ukazatel na zásobník instrukcí
* @return void
*/
void instructStackFree(tInstructStack *stack){
  tInstructStackItem *ptr;
  while (stack->top != NULL)
  {
    ptr = stack->top;
    stack->top = stack->top->next;
    free(ptr);
  }
}

/**
* Vloží instrukci na vrchol zásobníku
* @param stack Ukazatel na zásobník instrukcí
* @param stack Ukazatel na vkládanou instrukci
* @return int INST_SUCCESS v případě uspěchu a INST_ERROR v případě neúspěchu
*/
int instructPush(tInstructStack *stack, tListItem *addr){
  tInstructStackItem *newItem;
  if(newItem = malloc(sizeof (tInstructStackItem))){
    newItem->addr = addr;
    newItem->next = stack->top;
    stack->top = newItem;
    return INST_SUCCESS;
  }
  return INST_ERROR;
}

/**
* Odebere instrukci z vrcholu zásobníku a vrátí ji
* @param stack Ukazatel na zásobník instrukcí
* @return instructPop Ukazatel na instrukci, kterou jsme odebrali z vrcholu zásobníku
*/
tListItem *instructPop(tInstructStack *stack){
  tStackItem *ptr;
  table *tmp;
  if(stack->top != NULL){
    ptr = stack->top;
    tmp = stack->top->addr;
    stack->top = stack->top->next;
    free(ptr);
    return tmp;
  }
}

/**
* OPřečte instrukci z vrcholu zásobníku
* @param stack Ukazatel na zásobník instrukcí
* @return instrucTop Ukazatel na instrukci, kterou jsme přečetli
*/
tListItem *instrucTop(tInstructStack *stack){
  if(stack->top != NULL)
    return stack->top->addr;
}

/**
* Funkce zjistí, jestli je zásobník prázdný
* @param stack Ukazatel na zásobník instrukcí
* @return bool
*/
bool instructEmpty(tInstructStack *stack){
  if(stack->top == NULL)
    return true;
  return false;
}
