/* ********************************* ial.c ********************************** */
/* Soubor:              ial.c - Soubor pro                                    */
/*                                tabulku, radici a vyhledavaci algoritmus    */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:    -> Jan Herec             xherec00                        */
/*                   -> Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                   -> Michal Kovařík        xkovar70                        */
/* ************************************************************************** */


#include <stdio.h>
#include <stdlib.h>
#include "ial.h"
#include "constants.h"

/**
* Inicializuje Tabulku Symbolu
* @params: table*
* @return: NONE
*/
int newTable(table *t) {
    if(t == NULL){ // pokud sme dostali spatny ukazatel zahlas chybu
        return INTERNAL_ERROR;
    }
    else{ // pokud mame dobry ukazatel vycistime si vsechny prvky
        int i = 0;
        for(i; i < TABLESIZE; i++){
            (*t)[i] = NULL;
        }
    }
    return SUCCESS;
}

/**
* Generujue klic ze stringu
* @params: string*
* @return: int
*/
int getKey(string *s){
    int r = 1; //promenna ve ktere si budu ukladat hodnotu klice
    int i = 0;
    for(i; i < s->length; i++){
        r += s->str[i];
    }
    return (r % TABLESIZE);
}

/**
* Ulozi symbol do tabulky a vrati ukazatel na daný uložený symbol.
* @param t Ukazatel na TS, do které budeme symbol vkládat
* @param s Ukazatel na řetězec, který reprezentuje název symbolu
* @param identifierType Typ identifikátoru: VARIABLE nebo FUNKCE
* @param Error Chybový kód - příznak úspěšného nalezení, nebo neúspěšného nalezení symbolu
* @return tableItem* Vrací ukazatel na uložený symbol
*/
tableItem* saveItem(table *t, string *s, int identifierType, int *Error){
    // vytvoříme základní prostor pro symbol
    tableItem *item = malloc(sizeof(struct tableItem));
    if(item == NULL){
        *Error = INTERNAL_ERROR;
        return (tableItem*)NULL;
    }

    // vytvoříme prostor pro datovou složku symbolu
    Data *data = malloc(sizeof(struct Data));
    if(data == NULL){
        *Error = INTERNAL_ERROR;
        return NULL;
    }

    // vytvoříme prostor pro jméno symbolu
    string *name = malloc(sizeof(string));
    if(name == NULL){
        *Error = INTERNAL_ERROR;
        return NULL;
    }
    if(strInit(name) != STR_SUCCESS){
      *Error = INTERNAL_ERROR;
      return NULL;
    }

    if(strCpy(name, s)){
      *Error = INTERNAL_ERROR;
      return NULL;
    }

    // Vytvoříme prostor pro typy parametrů
    string *paramsTypes = malloc(sizeof(string));
    if(paramsTypes == NULL){
      *Error = INTERNAL_ERROR;
      return NULL;
    }
    if (strInit(paramsTypes) != STR_SUCCESS){
      *Error = INTERNAL_ERROR;
      return NULL;
    }

    // naplníme datovou složku symbolu
    data->name = name;
    data->initialized = false;
    data->paramsTypes = paramsTypes;
    data->paramsNames = NULL;
    data->identifierType = identifierType;
    data->declared = false;
    data->defined = false;
    data->localTS = NULL;
    data->firstInstructInFunc = NULL;
    data->returnInstruct = NULL;

    // naplníme základní položky symbolu
    item->key = getKey(s);
    item->data = data;
    item->next = NULL;

    // uložíme symbol na příslušnou pozici, indexovanou vygenrovaným klíčem
    if((*t)[item->key] != NULL){ // pokud uz s danym klicem neco existuje
        tableItem *temp = (*t)[item->key];
        tableItem **temp2 = &((*t)[item->key]);
        while(temp->next != NULL){ // projdeme az na konec
            temp = temp->next;
            temp2 = &((*temp2)->next);
        }
        (*temp2)->next = item; // a vytvorime dalsi synonymum
    }
    else{ // s timto klicem este nic nemame
        (*t)[item->key] = item; // tak to tam ulozime
    }

    *Error = SUCCESS;
    return item;
}

/**
* Vyhledá symbol v TS a vrátí jej,
* @param t Ukazatel na TS, do které budeme symbol vkládat
* @param s Ukazatel na řetězec, který reprezentuje název symbolu
* @param identifierType Typ identifikátoru: VARIABLE nebo FUNKCE
* @param Error Chybový kód - příznak úspěšného nalezení, nebo neúspěšného nalezení symbolu
* @return tableItem* Vrací ukazatel na nalezený symbol
*/
tableItem* searchItem(table *t, string *s, int identifierType, int *Error){
    int key = getKey(s);
    tableItem *item;

    if((*t)[key] != NULL){ // pokud se pro daný klíč v TS nachází alespoň jeden prvek, projedeme zřetězený seznam symbolů s daným klíčem
        item = (*t)[key];
        if(strCmp(s, item->data->name) != 0 || item->data->identifierType != identifierType){ // pokud první položka zeřtězeného seznamu není ta kterou hledáme
            while(item->next != NULL){ // projdeme celý zřětezěný seznam položek na daném indexu TS
                item = item->next;
                // pokud mají identifikátory shodné názvy a taky shodný typ, tak hledaný identifikátor v tabulce leží
                if(strCmp(s, item->data->name) == 0 && item->data->identifierType == identifierType){
                    *Error = SUCCESS;
                    return item; // FOUND
                }
            }
            *Error = NOT_FOUND;
            return (tableItem*)NULL; // 404 NOT FOUND
        }
        else{ // nalezli jsme položku
            *Error = SUCCESS;
            return item; // FOUND
        }
    }
    else{
        *Error = NOT_FOUND;
        return (tableItem*)NULL; // NOT FOUND
    }
}

/**
* Odstraní danou položku z TS
* @param i Ukazatel na položku v TS
* @return void
*/
void destroyItem(tableItem *i){
    strFree(i->data->name);
    free(i->data->name);

    // u proměnné která obsahuje řetezec, tento řetězec uvolníme
    if (i->data->dataType == DT_STRING && i->data->identifierType == VARIABLE && i->data->initialized == true){
      strFree(&(i->data->value.strVal));
    }

    // pokud řetězec obsahující typy parametrů je definován
    if (i->data->paramsTypes != NULL){
      strFree(i->data->paramsTypes);
      free(i->data->paramsTypes);
    }

    // pokud zřetězený seznam názvů parametrů není prázdný, tak jej smažeme
    if(i->data->paramsNames != NULL){
      ParamsNames *tmpParamsNamesPtr = i->data->paramsNames; // ukazovátko na parametr
      ParamsNames *tmpPtrForFree; // ukazovátko na parmaetr, který se bude rušit
      do{
        tmpPtrForFree = tmpParamsNamesPtr;
        tmpParamsNamesPtr = tmpParamsNamesPtr->next;

        // uvolníme paměť názvu parametru a struktury, která jej obaluje
        strFree(tmpPtrForFree->paramName);
        free(tmpPtrForFree->paramName);
        free(tmpPtrForFree);
      } while(tmpParamsNamesPtr != NULL);
    }

    free(i->data);

    free(i);
    return;
}

/**
* Odstraní danou položku z TS
* @param i Ukazatel na TS ve které se položka nachází
* @param i Ukazatel na položku v TS
* @return void
*/
void deleteItem(table *t, tableItem *i){
    tableItem *temp;
    if((*t)[i->key] != NULL){
        temp = (*t)[i->key];
    }
    else{
        return;
    }

    if(i == temp && temp->next == NULL){
        (*t)[i->key] = NULL;
        destroyItem(i);
        return;
    }
    else if(i == temp && temp->next != NULL){
        (*t)[i->key] = i->next;
        destroyItem(i);
        return;
    }
    else{
        while(temp->next != i){
            if(temp->next == NULL){
                return;
            }
            temp = temp->next;
        } // while
        temp->next = i->next;
        destroyItem(i);
    } // endIF
    return;
}

/**
* Odstraní zřetězený seznam položek na daném indexu tabulky
* @param i Ukazatel na 1. položku v zřetězeném seznamu
* @return void
*/
void disposeList(tableItem *i){
    // pokud je seznam prázdný, nebudeme pochopitelně nic mazat
    if (i == NULL){
      return;
    }
    tableItem *item;
    tableItem *itemNext = i;
    do{ // postupně každou položku seznamu smažeme
        item = itemNext;
        itemNext = item->next;
        if(item != NULL){
            destroyItem(item);
        }

    }while(itemNext != NULL);
    return;
}

/**
* Odstraní danou TS
* @param i Ukazatel na TS, kterou požadujeme smazat
* @return void
*/
void deleteTable(table *t){
    for(int i = 0; i < TABLESIZE; i++){ // pro každý index tabulky symbolů smaže zeřětězený seznam položek na tomto indexu
        disposeList((*t)[i]);
        (*t)[i] = NULL;
    }
    return;
}

/**
* Zkopíruje tabulku symbolů a vrátí ukazatel do paměti na tuto kopii
* @param tablePtr Ukazatel na tabulku, jejíž kopii budeme dělat
* @param Error Chybový kód - příznak úspěšně vytvořené kopie TS, nebo úspěšně vytvořené kopie
* @return void
*/
table* copyTable(table* tablePtr, int *Error){

  // vytvoříme a inicializujeme kostru kone kopie TS
  table *tableCopyPtr;
  tableCopyPtr = malloc(sizeof(table));
  if (newTable(tableCopyPtr) != SUCCESS) *Error = INTERNAL_ERROR;

  tableItem *item; // ukazovátko na položku v tabulce symbolů
  tableItem *lastItemCopy; // ukazovátko na poslední zkopírovanou položku TS

  // prohledáme všechny položky v TS
  for(int i = 0; i < TABLESIZE; i++){

    if ((*tablePtr)[i] != NULL){ // pokud na daném indexu TS existuje alespoň jeden prvek
      item = (*tablePtr)[i]; // nastavíme ukazovátko na první prvek zřetězeného seznamu
      int positionInListOfItems = 0; // inicializujeme pozici položky v zřetězeném seznamu

      do{ // prohledáváme zřetězený seznam, dokud se nedostanem na jeho konec
        // vytvoříme základní prostor pro symbol
        tableItem *itemCopy = malloc(sizeof(struct tableItem));
        if(itemCopy == NULL){
          *Error = INTERNAL_ERROR;
          return NULL;
        }

        // vytvoříme prostor pro datovou složku symbolu
        Data *dataCopy = malloc(sizeof(struct Data));
        if(dataCopy == NULL){
          *Error = INTERNAL_ERROR;
          return NULL;
        }

        // vytvoříme prostor pro jméno symbolu
        string *nameCopy = malloc(sizeof(string));
        if(nameCopy == NULL){
            *Error = INTERNAL_ERROR;
            return NULL;
        }
        if(strInit(nameCopy) != STR_SUCCESS){
          *Error = INTERNAL_ERROR;
          return NULL;
        }

        if(strCpy(nameCopy, item->data->name)){
          *Error = INTERNAL_ERROR;
          return NULL;
        }

        if (item->data->dataType == DT_STRING){ // inicializujeme proměnnou typu string
          if (strInit(&(dataCopy->value.strVal)) == STR_ERROR) return INTERNAL_ERROR;
        }
        // přiřadíme hodnoty proměnným, pokud byly inicializovány
        if (item->data->initialized && item->data->dataType == DT_STRING){
          strClr(&(dataCopy->value.strVal));
          if( strCatStr( &(dataCopy->value.strVal), &(item->data->value.strVal) ) ){
            return INTERNAL_ERROR;
          }
        }
        else if (item->data->initialized && item->data->dataType == DT_BOOLEAN){
          dataCopy->value.boolVal = item->data->value.boolVal;
        }
        else if (item->data->initialized && item->data->dataType == DT_INTEGER){
          dataCopy->value.intVal = item->data->value.intVal;
        }
        else if (item->data->initialized && item->data->dataType == DT_REAL){
          dataCopy->value.realVal = item->data->value.realVal;
        }

        // naplníme datovou složku symbolu
        dataCopy->name = nameCopy;
        dataCopy->initialized = item->data->initialized;
        dataCopy->paramsTypes = NULL;
        dataCopy->paramsNames = NULL;
        dataCopy->dataType = item->data->dataType;
        dataCopy->identifierType = item->data->identifierType;
        dataCopy->declared = item->data->declared;
        dataCopy->defined = item->data->defined;
        dataCopy->localTS = NULL;
        dataCopy->firstInstructInFunc = NULL;
        dataCopy->returnInstruct = NULL;

        // naplníme základní položky symbolu
        itemCopy->key = item->key;
        itemCopy->data = dataCopy;
        itemCopy->next = NULL;

        // pokud se jedná o první prvek zřetězeného seznamu, uložíme do TS odkaz na tento první prvek
        if (positionInListOfItems == 0){
          (*tableCopyPtr)[i] = itemCopy;

        }
        else{
          lastItemCopy->next = itemCopy; // nastavíme next minulé položky v zřetězeném seznamu na katuáýlně vytvořenou položku
        }

        lastItemCopy = itemCopy; // nastavíme ukazatel na posledení zkopírovanou položku na současnou položku
        positionInListOfItems++; // inkrementujeme pozici položky TS v zřetězeném seznamu položek na daném indexu

      } while((item = item->next) != NULL);
    }
    else{ // pokud na daném indexu neexistuje žádný prvek, tak nastvíme ukazatel na tomto indexu na NULL
      (*tableCopyPtr)[i] = NULL;
    }
  }

  *Error = SUCCESS;
  return tableCopyPtr;
}

/**
* Seradi zadany retezec "s"
*  -pouzita metoda "ShellSort"
*  -radi podle ordinalni hodnoty znaku od nejmensiho po nejvetsi
* @param  *s  Ukazatel na razeny retezec
*/
void strSort(char *s){
  int step, tmp, I, J, N;
  N = strlen(s);                // delka retezce

  step = N / 2;                 // velikost prvniho kroku
  while(step > 0){              // dokud je krok vetsi nez 0
    for(I = step; I < N; I++){  // cyklus pro paralelni N-tice
      J = I - step;             // index do retezce
      while(J >= 0 && s[J] > s[J + step]){ // bublinovy pruchod
        //printf("%s -> %c:=:%c", s, s[J], s[J + step]);
        tmp  = s[J];
        s[J] = s[J + step];     // vymena mensi a vetsi hodnoty
        s[J + step] = tmp;
        //printf(" -> %c,%c\n", s[J], s[J + step]);
        J = J - step;           // snizeni indexu o krok
      }
    }
    step = step / 2;            // puleni kroku
  }
}

/**
* Priprava pole charJump pro posuv vzorku
*  1.heuristika "Boyer-Moor algorithm"
* @param  *p          Ukazatel na hledany podretezec (pattern)
* @param  pLen        Delka retezce "p"
* @param  charJump[]  Pole pro posuv vzorku
*/
void prepCharJump(char *p, int pLen, int charJump[]){
  for(int i = 0; i < STR_SIZE; i++) // na vsechny indexy pole
    charJump[i] = pLen;             // vlozi delku hledaneho retezce

  for(int i = 0; i < pLen - 1; i++) // na index odpovidajici pismenu
    charJump[(unsigned char)p[i]] = pLen - i - 1; // vlozi spocitany posuv
}

/**
* Vyhleda prvni vyskyt podretezce "p" v retezci "t"
*  -pouzita metoda "Boyer-Moor algorithm"
*  -indexovano od 1 (prvni znak retezce ma pozici 1)
* @param   *t   Ukazatel na retezec (text)
* @param   *p   Ukazatel na hledany podretezec (pattern)
* @return  int  Pozice prvniho vyskytu podretezce
* @return  1    Pokud (p == NULL)
* @return  0    Pokud podretezec nebyl nalezen
*/
int strFind(char *t, char *p)
{
  if(t == NULL) return 0;
  if(p == NULL) return 1;

  int tLen = strlen(t);
  int pLen = strlen(p);
  int j = pLen;              // index do textu
  int k = pLen;              // index do vzorku
  int charJump[STR_SIZE];    // pole pro posuv vzorku 1.heuristika

  prepCharJump(p, pLen, charJump); // priprava pole 1.heuristiky

  while(j <= tLen && k > 0){ // dokud neprojde vsechno, nebo nenajde
    if(t[j-1] == p[k-1]){    // znaky se rovnaji
      j--;                   // v textu se presune o znak vlevo
      k--;                   // ve vzorku se presune o znak vlevo
    }else{                   // znaky se nerovnaji
      j = j + charJump[(unsigned char)t[j-1]]; // kvuli ceskym znakum
                             // v textu skoci o neco doprava
      k = pLen;              // ve vzorku se vrati uplne doprava
    }
  }

  if(k == 0){ // podretezec nalezen
    return j + 1;
  }else{      // podretezec NEnalezen
    return 0;
  }
}

/**
* Vrati podretezec zadaneho retezce "s"
* @param   *s        Ukazatel na retezec
* @param   position  Zacatek pozadovaneho podretezce
* @param   length    Delka pozadovaneho podretezce
* @return  char*     Serazeny retezec
*/
char *strCopy(char *str, int position, int length) {
  if (position <= 0){ // pokud zadal uživatel pozici zápornou nebo 0, nastavíme pozici na 1
    position = 1;
  }
  if (position + length - 1 > strlen(str)){ // pokud zadal uživatel délku řetezce za hranicemi původního řetezce, tak vybereme podřetězec až k této hranici
    length = strlen(str) - position + 1;
  }
  if (position > strlen(str) || length < 0){ // pokud zadal uživatel pozici v řetezci za hranicemi původního řetězce, nebo nastavil délku zápornou, vrátíme prázdný řetezec
    length = 0;
  }

  // position je pocitano od jednicky, proto je nutna dekrementace (prvni znak stringu = nulta pozice v poli)
  position--;
  // vytvoreni pole pro hledany substring
  char *newArray = malloc((length + 1) * sizeof(char));
  // pruchod puvodniho stringu (od pozice position) o delce pozadovaneho substringu
  // a zkopirovani techto hodnot do hledaneho substringu
  for (int i = 0; i < length; i++) {
    newArray[i] = str[position + i];
  }
  newArray[length] = '\0';
  // navrat hledaneho substringu
  return newArray;
}
