all:
	gcc main.c expr.c interpret.c instructions.c parser.c ial.c scanner.c str.c -o program -w -Wall -Wextra -pedantic -std=c99

clean:
	rm -f *.o
