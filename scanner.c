/* ******************************* scanner.c ******************************** */
/* Soubor:              scanner.c - Lexikalni analyzator (scanner)            */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#include <stdio.h>
#include <ctype.h>
#include "scanner.h"

// tisk chyboveho hlaseni
#define ERROR_PRINT \
  fprintf(stderr, "%s:%d:%d: lexical error\n", fileName, errRow, errChar);

/* ************************ Stavy konecneho automatu ************************ */
#define ST_START         0
#define ST_ZERO          1
#define ST_COMMENT       2
#define ST_ID_KW_DT      3
#define ST_INT_RE        4
#define ST_RE_FRACT      5
#define ST_FRACT         6
#define ST_RE_EXP        7
#define ST_EXP_SGN       8
#define ST_EXP           9
#define ST_STRING        10
#define ST_STR_ESC_APOST 11
#define ST_ESC           12
#define ST_ESC_1         13
#define ST_ESC_2         14
#define ST_ESC_3         15
#define ST_COLON         16
#define ST_LESS          17
#define ST_MORE          18

/* ************************ Deklarace a tela funkci ************************* */
FILE *source;   // zdrojovy soubor
char *fileName; // jmeno zdrojoveho souboru
errRow  = 1;    // inicializace pozice chybneho znaku
errChar = 0;

/**
* Nastavi zdrojovy soubor
* @param  *f  Ukazatel na zdrojovy soubor
*/
void setSourceFile(FILE *f){
  source = f;
}

/**
* Nastavi jmeno souboru
* @param  *f  Ukazatel na zdrojovy soubor
*/
void setFileName(char *f){
  fileName = f;
}

/**
* Otestuje, zda bylo zadano KLICOVE SLOVO nebo DATOVY TYP
*  -> pokud nebylo zadano ani jedno, byl zadan IDENTIFIKATOR
* @param  *token     Ukazatel na token
* @return  int >= 0  Cislo odpovidajici typu tokenu
*/
int isKeyword(string *token){
  if(strCmpConst(token, "BEGIN") == 0){
    return KW_BEGIN;
  }else if(strCmpConst(token, "END") == 0){
    return KW_END;
  }else if(strCmpConst(token, "VAR") == 0){
    return KW_VAR;
  }else if(strCmpConst(token, "IF") == 0){
    return KW_IF;
  }else if(strCmpConst(token, "THEN") == 0){
    return KW_THEN;
  }else if(strCmpConst(token, "ELSE") == 0){
    return KW_ELSE;
  }else if(strCmpConst(token, "DO") == 0){
    return KW_DO;
  }else if(strCmpConst(token, "WHILE") == 0){
    return KW_WHILE;
  }else if(strCmpConst(token, "TRUE") == 0){
    return KW_TRUE;
  }else if(strCmpConst(token, "FALSE") == 0){
    return KW_FALSE;
  }else if(strCmpConst(token, "FUNCTION") == 0){
    return KW_FUNCTION;
  }else if(strCmpConst(token, "FORWARD") == 0){
    return KW_FORWARD;
  }else if(strCmpConst(token, "READLN") == 0){
    return KW_READLN;
  }else if(strCmpConst(token, "WRITE") == 0){
    return KW_WRITE;
  }else if(strCmpConst(token, "SORT") == 0){
    return KW_SORT;
  }else if(strCmpConst(token, "FIND") == 0){
    return KW_FIND;
  }else if(strCmpConst(token, "INTEGER") == 0){
    return DT_INTEGER;
  }else if(strCmpConst(token, "REAL") == 0){
    return DT_REAL;
  }else if(strCmpConst(token, "STRING") == 0){
    return DT_STRING;
  }else if(strCmpConst(token, "BOOLEAN") == 0){
    return DT_BOOLEAN;
  }else{
    return ID;
  }
}

/**
* Vrati dalsi token
*  -> informace o automatu v grafu FSM
* @param  *token     Ukazatel na token
* @return  int >= 0  Cislo odpovidajici typu tokenu
* @return  -1        Lexikalni chyba
* @return  -99       Interni chyba
*/
int getToken(string *token){
  int state = ST_START; // aktualni stav automatu
  int c;                // nacteny znak
  int esc;              // hodnota escape sekvence
  strClr(token);        // vycisti token

  while(1){             // prochazi vstupni soubor
    c = getc(source);   // nacte z nej znak

    if(c == '\n'){      // pocitani pozice chybneho znaku
      errRow++;
      errChar = 0;
    }else{
      errChar++;
    }

    switch(state){ // FSM - konecny automat

      case ST_START:
        if(isspace(c)){
          state = ST_START;
        }else if(c == '0'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_ZERO;
        }else if(c == '{'){
          state = ST_COMMENT;
        }else if(isalpha(c) || c == '_'){
          if(strCat(token, toupper(c))) return INTERNAL_ERROR;
          state = ST_ID_KW_DT;
        }else if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_INT_RE;
        }else if(c == '\''){
          state = ST_STRING;
        }else if(c == ':'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_COLON;
        }else if(c == '<'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_LESS;
        }else if(c == '>'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_MORE;
        }else if(c == '+'){
          return OP_ADD;
        }else if(c == '-'){
          return OP_SUB;
        }else if(c == '*'){
          return OP_MUL;
        }else if(c == '/'){
          return OP_DIV;
        }else if(c == '='){
          return OP_EQUAL;
        }else if(c == ';'){
          return SEMICOLON;
        }else if(c == '('){
          return BRACKET_L;
        }else if(c == ')'){
          return BRACKET_R;
        }else if(c == '.'){
          return DOT;
        }else if(c == ','){
          return COMA;
        }else if(c == EOF){
          return END;
        }else{ // nesedi zadne pravidlo -> lexical error
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_ZERO:
        if(c == '0'){        // ignoruje pocatecni nuly
          state = ST_ZERO;
        }else if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_INT_RE;
        }else if(c == '.'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_RE_FRACT;
        }else if(c == 'e' || c == 'E'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_RE_EXP;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return LIT_INTEGER;
        }
        break;

      case ST_COMMENT:
        if(c == '}'){       // konec komentare
          state = ST_START; // vrati se na start
        }else if(c == EOF){ // neukonceny komentar -> lexical error
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_ID_KW_DT:
        if(isalnum(c) || c == '_'){
          if(strCat(token, toupper(c))) return INTERNAL_ERROR;;
        }else{

          if(c == '\n'){           // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source);       // vrati nacteny znak pro znovuzpracovani
          return isKeyword(token); // zjisti co bylo nacteno ID/KW/DT
        }
        break;

      case ST_INT_RE:
        if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
        }else if(c == '.'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_RE_FRACT;
        }else if(c == 'e' || c == 'E'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_RE_EXP;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return LIT_INTEGER;
        }
        break;

      case ST_RE_FRACT:
        if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_FRACT;
        }else{
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_FRACT:
        if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
        }else if(c == 'e' || c == 'E'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_RE_EXP;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return LIT_REAL;
        }
        break;

      case ST_RE_EXP:
        if(c == '+' || c == '-'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_EXP_SGN;
        }else if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_EXP;
        }else{
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_EXP_SGN:
        if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_EXP;
        }else{
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_EXP:
        if(isdigit(c)){
          if(strCat(token, c)) return INTERNAL_ERROR;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return LIT_REAL;
        }
        break;

      case ST_STRING:
        if(c == '\''){ // zachycen apostrov
          state = ST_STR_ESC_APOST;
        }else if(c > 31){
          if(strCat(token, c)) return INTERNAL_ERROR;
        }else{
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_STR_ESC_APOST: // LIT_STRING / ESCAPE sekvence / APOSTROV
        if(c == '\''){
          if(strCat(token, c)) return INTERNAL_ERROR;
          state = ST_STRING;
        }else if(c == '#'){
          state = ST_ESC;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return LIT_STRING;
        }
        break;

      case ST_ESC:         // prvni cislo ESC sekv.
        if(c == '0'){      // ignoruje pocatecni nuly
          state = ST_ESC;
        }else if(isdigit(c)){
          esc = (c - '0'); // prevede retezec na cislo
        //printf("c->%c esc->%d\n", c, esc);
          state = ST_ESC_1;
        }else{
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_ESC_1:                // druhe cislo ESC sekv.
        if(c == '\''){              // konec ESC
          if(strCat(token, esc)) return INTERNAL_ERROR;
          state = ST_STRING;
        }else if(isdigit(c)){
          esc = esc*10 + (c - '0'); // prevede retezec na cislo
        //printf("c->%c esc->%d\n", c, esc);
          state = ST_ESC_2;
        }else{
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_ESC_2:                // treti cislo ESC sekv.
        if(c == '\''){              // konec ESC
          if(strCat(token, esc)) return INTERNAL_ERROR;
          state = ST_STRING;
        }else if(isdigit(c)){
          esc = esc*10 + (c - '0'); // prevede retezec na cislo
        //printf("c->%c esc->%d\n", c, esc);
          state = ST_ESC_3;
        }else{
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_ESC_3: // kontrola rozsahu nactene ESC sekv.
        if(c == '\'' && esc <= 255){
          if(strCat(token, esc)) return INTERNAL_ERROR;
          state = ST_STRING;
        }else{       // pokud neni validni -> lexical error
          ERROR_PRINT
          return SCAN_ERROR;
        }
        break;

      case ST_COLON:
        if(c == '='){
          if(strCat(token, c)) return INTERNAL_ERROR;
          return OP_ASSIGN;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return COLON;
        }
        break;

      case ST_LESS:
        if(c == '>'){
          if(strCat(token, c)) return INTERNAL_ERROR;
          return OP_LESS_MORE;
        }else if(c == '='){
          if(strCat(token, c)) return INTERNAL_ERROR;
          return OP_LESS_EQUAL;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return OP_LESS;
        }
        break;

      case ST_MORE:
        if(c == '='){
          if(strCat(token, c)) return INTERNAL_ERROR;
          return OP_MORE_EQUAL;
        }else{

          if(c == '\n'){     // uprava pozice chybneho znaku
            errRow--;
          }else{
            errChar--;
          }

          ungetc(c, source); // vrati nacteny znak pro znovuzpracovani
          return OP_MORE;
        }
        break;
    }
  }
}
