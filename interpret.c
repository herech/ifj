/* ****************************** interpret.c ******************************* */
/* Soubor:              interpret.c - Interpter vnitrniho kodu                */
/* Kodovani:            UTF-8                                                 */
/* Datum:               listopad.2014                                         */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#include "interpret.h"
#include <stdio.h>
#include <string.h>
#include <malloc.h>

/**
* Makro inicializuje struktury pouzivane interpretem
*/
#define INT_RES_INIT          \
  instStackInit(&frameStack); \
  strInit(&retInstr);         \
  strInit(&retVal);           \
  strInit(&pomStr);

/**
* Makro uvolni struktury pouzivane interpretem
*/
#define INT_RES_FREE  \
  strFree(&retInstr); \
  strFree(&retVal);   \
  strFree(&pomStr);   \
  instStackFree(&frameStack);

// tisk chyboveho hlaseni
#define ERROR_PRINT_U \
  fprintf(stderr, "ERROR -> Uninicialized variable: %s\n", errVar);

char *errVar; // jmeno promenne do vypisu

/**
* Interpretuje 3AK
* @param  *t   Ukazatel na tabulku symbolu
* @param  *l   Ukazatel na seznam instrukci
* @param  *m   Ukazatel na prvni instrukci fce Main()
* @return  0   Vse v poradku
* @return  6   Behova chyba - chyba nacteni cisla ze vstupu
* @return  7   Behova chyba - Uninicialized variable
* @return  8   Behova chyba - Division by zero
* @return  9   Behova chyba - Else
* @return  99  Interni chyba
*/
int interpret(table *t, tListOfInstr *l, struct listItem *m){
  tStack frameStack;     // zasobnik pro ramce (zkopirovane lokalni TS)
  table *stackTab;       // ramec na vrcholu zasobniku
  table *prepTab;        // ramec pripravovany pro push() na zasobnik
  tStackItem *destrItem; // mazany ramec (item zasobniku)
  tInstr *I;             // instrukce
  int Err;               // promenna pro error z searchItem()
  double op1, op2;       // promenne pro aritmeticke operace
  void *addr;            // pomocna adresa
  Data *addr1;           // ukazatel na data TS (operand 1)
  Data *addr2;           // ukazatel na data TS (operand 2)
  Data *addr3;           // ukazatel na data TS (operand 3)
  bool nextInstr = true; // priznak jestli prejit na dalsi instrukci
  string retInstr;       // navratova instrukce z funkce
  string retVal;         // navratova hodnota funkce
  string pomStr;         // pomocny string
  Data *(vestFunc[3]);   // pole pro parametry vestavenych funkci
  int vestFuncI = 0;     // indexace pole vestFunc[]

  INT_RES_INIT           // inicializace struktur

  if( strCatChar(&retInstr, "$returnAddress") ){
    INT_RES_FREE
    return INTERN_ERROR;
  }
  if( strCatChar(&retVal, "$returnValue") ){
    INT_RES_FREE
    return INTERN_ERROR;
  }

  listGoto(l, m);        // nastavi active na prvni instrukci fce Main()

  while(1){              // prochazi list instrukci
    I = listGetData(l);  // dostane instrukci
    switch (I->instType) // podle instrukce rozhodne co ma delat
    {
    case I_HALT:         // ukoncovaci instrukce
      INT_RES_FREE       // uklidi a uspesne skonci
      return 0;

      break;

    case I_NOP:          // nic nedela

      break;

/* *********************** Interpretace volani funkci *********************** */
    case I_CREATE_FRAME: // vytvori ramec volane funkce (zkopirovana lokalni TS)
      addr = searchItem(t, ((Data*)I->addr1)->name, FUNCTION, &Err);
      addr1 = ((tableItem*)addr)->data;          // najde volanou funkci

      prepTab = copyTable(addr1->localTS, &Err); // zkopiruje jeji lokalni TS
      if (Err == INTERNAL_ERROR) return INTERN_ERROR;

      break;

    case I_CALL: // volani funkce
      addr = searchItem(t, ((Data*)I->addr1)->name, FUNCTION, &Err);
      addr1 = ((tableItem*)addr)->data; // najde volanou funkci

      addr = searchItem(prepTab, &retInstr, VARIABLE, &Err);
      addr3 = ((tableItem*)addr)->data; // najde misto pro navratovou instrukci

      addr3->returnInstruct = l->active->nextItem; // ulozi navratovou instrukci

      push(&frameStack, prepTab);  // vlozi ramec na zasobnik
      stackTab = top(&frameStack); // ulozi si ukazatel na tento ramec
      prepTab = NULL;              // zadny ramec uz se nepripravuje

      nextInstr = false;           // nakonci se nema prejit na dalsi instrukci
      listGoto(l, addr1->firstInstructInFunc); // skok na prvni instrukci funkce

      break;

    case I_CALL_VEST: // volani vestavenych funkci
      addr = searchItem(t, ((Data*)I->addr1)->name, FUNCTION, &Err);
      addr1 = ((tableItem*)addr)->data; // najde volanou funkci

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(strcmp(addr1->name->str, "LENGTH") == 0){
        addr3->value.intVal = strlen( vestFunc[0]->value.strVal.str );

      }else if(strcmp(addr1->name->str, "SORT") == 0){
        strClr(&pomStr); // zkopiruje neserazeny retezec do pomocne promenne
        if( strCatStr( &pomStr, &(vestFunc[0]->value.strVal) ) ){
          INT_RES_FREE
          return INTERN_ERROR;
        }

        strClr( &(addr3->value.strVal) );   // zkopiruje neserazeny retezec do promenne pro vysledek
        if( strCatStr( &(addr3->value.strVal), &pomStr ) ){
          INT_RES_FREE
          return INTERN_ERROR;
        }

        strSort( addr3->value.strVal.str ); // seradi retezec v promenne pro vysledek

      }else if(strcmp(addr1->name->str, "FIND") == 0){
        addr3->value.intVal = strFind( vestFunc[0]->value.strVal.str, vestFunc[1]->value.strVal.str );

      }else if(strcmp(addr1->name->str, "COPY") == 0){
        strClr(&pomStr); // zkopiruje retezec do pomocne promenne
        if( strCatStr( &pomStr, &(vestFunc[0]->value.strVal) ) ){
          INT_RES_FREE
          return INTERN_ERROR;
        }

        strFree( &(addr3->value.strVal) ); // zkopiruje pozadovanou cast retezce a nastavi delku a allocSize
        addr3->value.strVal.str = strCopy( pomStr.str, vestFunc[1]->value.intVal, vestFunc[2]->value.intVal );
        addr3->value.strVal.length = strlen( addr3->value.strVal.str );
        addr3->value.strVal.allocSize = strlen( addr3->value.strVal.str );

      }

      addr3->initialized = true;
      vestFuncI = 0; // index do pole parametru vestavenych fci

      break;

    case I_SET_PAR_VEST:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      vestFunc[vestFuncI] = addr1; // ulozi parametr do pole parametru
      vestFuncI++;                 // index do pole parametru vestavenych fci

      break;

    case I_SET_PAR:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      addr = searchItem(prepTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
      addr3 = ((tableItem*)addr)->data; // najde misto pro parametr ve vytvarenem ramci

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        addr3->value.intVal = addr1->value.intVal;

      }else if(addr1->dataType == DT_REAL){
        addr3->value.realVal = addr1->value.realVal;

      }else if(addr1->dataType == DT_BOOLEAN){
        addr3->value.boolVal = addr1->value.boolVal;

      }else if(addr1->dataType == DT_STRING){
        strClr( &(addr3->value.strVal) );
        if( strCatStr( &(addr3->value.strVal), &(addr1->value.strVal) ) ){
          INT_RES_FREE
          return INTERN_ERROR;
        }
      }
      addr3->initialized = true;

      break;

    case I_RETURN: // navrat z funkce
      addr = searchItem(stackTab, &retInstr, VARIABLE, &Err);
      addr1 = ((tableItem*)addr)->data;   // najde navratovou instrukci

      nextInstr = false; // nakonci se nema prejit na dalsi instrukci
      listGoto(l, addr1->returnInstruct); // prejde na ni

      break;

    case I_SET_RESULT_AND_DESTROY_FRAME: // preda navratovou hodnotu a smaze ramec
      addr = searchItem(stackTab, &retVal, VARIABLE, &Err);
      addr1 = ((tableItem*)addr)->data;  // najde navratovou hodnotu

      destrItem = pop(&frameStack); // ulozi ukazatel na mazany prvek a vyhodi ho ze zasobniku
      stackTab = top(&frameStack);  // novy vrchol zasobniku

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        addr3->value.intVal = addr1->value.intVal;

      }else if(addr1->dataType == DT_REAL){
        addr3->value.realVal = addr1->value.realVal;

      }else if(addr1->dataType == DT_BOOLEAN){
        addr3->value.boolVal = addr1->value.boolVal;

      }else if(addr1->dataType == DT_STRING){
        strClr( &(addr3->value.strVal) );
        if( strCatStr( &(addr3->value.strVal), &(addr1->value.strVal) ) ){
          INT_RES_FREE
          return INTERN_ERROR;
        }
      }
      addr3->initialized = true; // preda navratovou hodnotu

      itemFree(destrItem);       // skutecne smaze mazanyy ramec

      break;

/* ******************** Interpretace ridicich instrukci ********************* */
    case I_ASSIGN:

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        addr3->value.intVal = addr1->value.intVal;

      }else if(addr1->dataType == DT_REAL){
        addr3->value.realVal = addr1->value.realVal;

      }else if(addr1->dataType == DT_BOOLEAN){
        addr3->value.boolVal = addr1->value.boolVal;

      }else if(addr1->dataType == DT_STRING){
        strClr(&pomStr);
        if( strCatStr( &pomStr, &(addr1->value.strVal) ) ){
          INT_RES_FREE
          return INTERN_ERROR;
        }

        strClr( &(addr3->value.strVal) );
        if( strCatStr( &(addr3->value.strVal), &pomStr ) ){
          INT_RES_FREE
          return INTERN_ERROR;
        }
      }
      addr3->initialized = true;

      break;

    case I_IF: // pokud true pokracuje, pokud false skoci do bloku ELSE
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->value.boolVal == false){
        nextInstr = false;     // nakonci se nema prejit na dalsi instrukci
        listGoto(l, I->addr3); // skoci na blok ELSE
      }

      break;

    case I_ELSE: // blok IF se vykonal, blok ELSE se preskoci
      nextInstr = false;     // nakonci se nema prejit na dalsi instrukci
      listGoto(l, I->addr3); // skoci za blok else

      break;

    case I_WHILE: // pokud true pokracuje, pokud false skoci za telo WHILE
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->value.boolVal == false){
        nextInstr = false;     // nakonci se nema prejit na dalsi instrukci
        listGoto(l, I->addr3); // skoci za telo WHILE
      }

      break;

    case I_REPEAT_WHILE: // skoci na vyhodnoceni podminky u WHILE
      nextInstr = false; // nakonci se nema prejit na dalsi instrukci
      listGoto(l, I->addr3);

      break;

    case I_READLN:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr3->dataType == DT_INTEGER){
        if( (scanf("%d", &(addr3->value.intVal))) == 0 ){
          INT_RES_FREE
          errVar = addr3->name->str;
          fprintf(stderr, "ERROR -> Integer value expected  %s : integer;\n", errVar);
          return RUN_N_ERROR;
        }

      }else if(addr3->dataType == DT_REAL){
        if( (scanf("%lg", &(addr3->value.realVal))) == 0 ){
          INT_RES_FREE
          errVar = addr3->name->str;
          fprintf(stderr, "ERROR -> Real value expected  %s : real;\n", errVar);
          return RUN_N_ERROR;
        }

      }else if(addr3->dataType == DT_STRING){
        int c;
        strClr( &(addr3->value.strVal) );
        c = getchar();

        if(c == '\n')
          c = getchar();

        while( (c != '\n') && (c != EOF) ){
          if( strCat(&(addr3->value.strVal), c) ){
            INT_RES_FREE
            return INTERN_ERROR;
          }
          c = getchar();
        }
      }
      addr3->initialized = true;

      break;

    case I_WRITE:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        printf("%d", addr1->value.intVal);

      }else if(addr1->dataType == DT_REAL){
        printf("%g", addr1->value.realVal);

      }else if(addr1->dataType == DT_BOOLEAN){
        if(addr1->value.boolVal == true){
          printf("%s", "true");
        }else{
          printf("%s", "false");
        }

      }else if(addr1->dataType == DT_STRING){
        printf("%s", strGet( &(addr1->value.strVal) ));
      }

      break;

/* ******************* Interpretace instrukci pro vyrazy ******************** */
    case I_ADD:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        op1 = addr1->value.intVal;
      }else{
        op1 = addr1->value.realVal;
      }

      if(addr2->dataType == DT_INTEGER){
        op2 = addr2->value.intVal;
      }else{
        op2 = addr2->value.realVal;
      }

      if(addr3->dataType == DT_INTEGER){
        addr3->value.intVal = op1 + op2;
      }else{
        addr3->value.realVal = op1 + op2;
      }
      addr3->initialized = true;

      break;

    case I_STR_CONCAT:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      strClr( &(addr3->value.strVal) );

      if( strCatStr( &(addr3->value.strVal), &(addr1->value.strVal) ) ){
        INT_RES_FREE
        return INTERN_ERROR;
      }
      if( strCatStr( &(addr3->value.strVal), &(addr2->value.strVal) ) ){
        INT_RES_FREE
        return INTERN_ERROR;
      }

      addr3->initialized = true;

      break;

    case I_SUB:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        op1 = addr1->value.intVal;
      }else{
        op1 = addr1->value.realVal;
      }

      if(addr2->dataType == DT_INTEGER){
        op2 = addr2->value.intVal;
      }else{
        op2 = addr2->value.realVal;
      }

      if(addr3->dataType == DT_INTEGER){
        addr3->value.intVal = op1 - op2;
      }else{
        addr3->value.realVal = op1 - op2;
      }
      addr3->initialized = true;

      break;

    case I_MUL:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        op1 = addr1->value.intVal;
      }else{
        op1 = addr1->value.realVal;
      }

      if(addr2->dataType == DT_INTEGER){
        op2 = addr2->value.intVal;
      }else{
        op2 = addr2->value.realVal;
      }

      if(addr3->dataType == DT_INTEGER){
        addr3->value.intVal = op1 * op2;
      }else{
        addr3->value.realVal = op1 * op2;
      }
      addr3->initialized = true;

      break;

    case I_DIV:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        op1 = addr1->value.intVal;
      }else{
        op1 = addr1->value.realVal;
      }

      if(addr2->dataType == DT_INTEGER){
        op2 = addr2->value.intVal;
      }else{
        op2 = addr2->value.realVal;
      }

      if(op2 == 0){
        INT_RES_FREE
        fprintf(stderr, "ERROR -> Division by zero  %s := 0\n", addr2->name->str);
        return RUN_D_ERROR;
      }

      if(addr3->dataType == DT_INTEGER){
        addr3->value.intVal = op1 / op2;
      }else{
        addr3->value.realVal = op1 / op2;
      }
      addr3->initialized = true;

      break;

    case I_LESS:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }

      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        if(addr1->value.intVal < addr2->value.intVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_REAL){
        if(addr1->value.realVal < addr2->value.realVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_BOOLEAN){
        if(addr1->value.boolVal < addr2->value.boolVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_STRING){
        if( (strCmp( &(addr1->value.strVal), &(addr2->value.strVal) )) < 0){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }
      }
      addr3->initialized = true;

      break;

    case I_MORE:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        if(addr1->value.intVal > addr2->value.intVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_REAL){
        if(addr1->value.realVal > addr2->value.realVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_BOOLEAN){
        if(addr1->value.boolVal > addr2->value.boolVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_STRING){
        if( (strCmp( &(addr1->value.strVal), &(addr2->value.strVal) )) > 0){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }
      }
      addr3->initialized = true;

      break;

    case I_LESS_EQUAL:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        if(addr1->value.intVal <= addr2->value.intVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_REAL){
        if(addr1->value.realVal <= addr2->value.realVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_BOOLEAN){
        if(addr1->value.boolVal <= addr2->value.boolVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_STRING){
        if( (strCmp( &(addr1->value.strVal), &(addr2->value.strVal) )) <= 0){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }
      }
      addr3->initialized = true;

      break;

    case I_MORE_EQUAL:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        if(addr1->value.intVal >= addr2->value.intVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_REAL){
        if(addr1->value.realVal >= addr2->value.realVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_BOOLEAN){
        if(addr1->value.boolVal >= addr2->value.boolVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_STRING){
        if( (strCmp( &(addr1->value.strVal), &(addr2->value.strVal) )) >= 0){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }
      }
      addr3->initialized = true;

      break;

    case I_EQUAL:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        if(addr1->value.intVal == addr2->value.intVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_REAL){
        if(addr1->value.realVal == addr2->value.realVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_BOOLEAN){
        if(addr1->value.boolVal == addr2->value.boolVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_STRING){
        if( (strCmp( &(addr1->value.strVal), &(addr2->value.strVal) )) == 0){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }
      }
      addr3->initialized = true;

      break;

    case I_LESS_MORE:
      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr1)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr1 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr1)->name, VARIABLE, &Err);
        addr1 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr2)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr2 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr2)->name, VARIABLE, &Err);
        addr2 = ((tableItem*)addr)->data;
      }

      if(empty(&frameStack) == false){      // zasobnik neni prazdny
        addr = searchItem(stackTab, ((Data*)I->addr3)->name, VARIABLE, &Err);
        if(Err != NOT_FOUND)
          addr3 = ((tableItem*)addr)->data; // zkusi najit v TS na vrcholu zasobniku
      }else{                                // zasobnik prazdny -> nenasel
        Err = NOT_FOUND;
      }
      if(Err == NOT_FOUND){ // nenalezeno na zasobniku hleda v globalni TS
        addr = searchItem(t, ((Data*)I->addr3)->name, VARIABLE, &Err);
        addr3 = ((tableItem*)addr)->data;
      }

      if(addr1->initialized == false){
        INT_RES_FREE
        errVar = addr1->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr2->initialized == false){
        INT_RES_FREE
        errVar = addr2->name->str;
        ERROR_PRINT_U
        return RUN_U_ERROR; // Uninicialized variable
      }

      if(addr1->dataType == DT_INTEGER){
        if(addr1->value.intVal != addr2->value.intVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_REAL){
        if(addr1->value.realVal != addr2->value.realVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_BOOLEAN){
        if(addr1->value.boolVal != addr2->value.boolVal){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }

      }else if(addr1->dataType == DT_STRING){
        if( (strCmp( &(addr1->value.strVal), &(addr2->value.strVal) )) != 0){
          addr3->value.boolVal = true;
        }else{
          addr3->value.boolVal = false;
        }
      }
      addr3->initialized = true;

      break;
    }

    if(nextInstr == true){ // pokud se neprovedl skok
      listNext(l);         // prejde na dalsi instrukci
    }else{
      nextInstr = true;
    }
  }
}
